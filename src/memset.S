	[bits 64]
	global memset:function
	section .text

memset:
	;; prolog

	push rbp
	mov rbp, rsp
	mov rcx, 0

start_loop:
	cmp rcx, rdx
	je end_loop
	mov [rdi], rsi
	add rdi, 1
	add rcx, 1
	jmp start_loop

end_loop:
	;; epilog
	mov rax, rdi
	mov rsp, rbp
	pop rbp
	ret
